import React, { Component, useContext  } from 'react';
import {Container, Content, Button, Text, Body, View, Item, Input, Label, CheckBox, Card, CardItem, Left, Footer, Icon, Spinner} from 'native-base';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import style, {headColor, greyColor, noImageIcon} from './../../layout/style';
import { TouchableOpacity, StyleSheet, TextInput, Image, PermissionsAndroid} from 'react-native';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import ImagePicker from 'react-native-image-picker';
import Modal from 'react-native-modal';
import Geolocation from '@react-native-community/geolocation';
import {Dimensions} from 'react-native';

import AuthContext,{ AuthProvider } from "./../../providers/AuthProvider";
import TopHeader from './../../layout/TopHeader';
import Loader from './../../layout/Loader';
import { profileProvider } from '../../providers/ProfileProvider';

const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;
export default class RegisterScreen extends  React.Component {
    static contextType = AuthContext;
    clockTimer = null;

    constructor(props) {
        super(props);
        this._name = React.createRef();
        this._address = React.createRef();
        this._selVillageOutlet = React.createRef();
        this._selOutletType = React.createRef();
        this._selOutletScale = React.createRef();
        this._siup_number = React.createRef();
        this._phone_no_1 = React.createRef();
        this._postal_code = React.createRef();
        this._pic_name = React.createRef();
        this._pic_address = React.createRef();
        this._selVillagePIC = React.createRef();
        this._pic_postal_code = React.createRef();
        this._selIdType = React.createRef();
        this._pic_id_number = React.createRef();
        this._pic_phone_no = React.createRef();
        this._searchInput = React.createRef();

        this.state = {
            currentTab: 'outlet',
            loading: false,
            errorModal: false,
            errorMessage: null,
            successModal: false,
            image_1 : null,
            image_2 : null,
            imageWidth_1: '100%',
            imageHeight_1: 100,
            imageWidth_2: '100%',
            imageHeight_2: 100,
            image_1_dirty: false,
            image_2_dirty: false,
            deleteImage: null,
            exist_image_1: false,
            exist_image_2: false,
            remove_image_1: false,
            remove_image_2: false,

            searchVillage: "",
            pageSearch: 1,
            loadingSearch: false,
            noMoreLoad: false,
            enableScroll: false,
            errorSearch: null,
            selVillageOutlet: null,

            selVillagePIC: null,

            viewLocationModal: false,
            currentLocationSearch: null,
            currentLocationData: null,

            viewIdTypeModal: false,
            selIdType: null,
            selIdTypeText: '',

            viewOutletTypeModal: false,
            selOutletType: null,
            selOutletTypeText: '',

            viewOutletScaleModal: false,
            selOutletScale: null,
            selOutletScaleText: '',
            profileData: {},
            searchTimer: 1

        };

        this.loadProfile = this.loadProfile.bind(this);

    }

    componentDidMount() {
        this.loadProfile();

    }

    requestLocation = async () => {
        try {
            const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                {
                    title: 'App Location Permission',
                    message: 'App needs access to your location ',
                    buttonNeutral: 'Ask Me Later',
                    buttonNegative: 'Cancel',
                    buttonPositive: 'OK',
                },
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                console.log('You can use the location', granted);
                Geolocation.getCurrentPosition(
                    (info) => this.setState({profileData: {...this.state.profileData, meta_lat: info.coords.latitude, meta_long: info.coords.longitude, locationSet: true}}),
                    (error) => {
                        const message = 
                        (<Text style={{ color: 'red', flexWrap: 'wrap', fontSize: 14, textAlign: 'left'}} > Gagal mendapatkan lokasi, Mohon periksa pengaturan layanan lokasi</Text>);
                        this.setState({loading: false, errorModal: true, errorMessage: message});
                        return;
                    }
                )
            } else {
                const message = 
                (<Text style={{ color: 'red', flexWrap: 'wrap', fontSize: 14, textAlign: 'left'}} > Izin akses lokasi tidak diberikan</Text>);
                this.setState({loading: false, errorModal: true, errorMessage: message});
                return;
            }
        } catch (err) {
            const message = 
            (<Text style={{ color: 'red', flexWrap: 'wrap', fontSize: 14, textAlign: 'left'}} > Gagal mendapatkan lokasi, Mohon periksa pengaturan layanan lokasi</Text>);
            this.setState({loading: false, errorModal: true, errorMessage: message});
            return;
        }
    };

    async loadProfile() {
        this.setState({profileData: {}, loading: true}, async () => {
            const result = await profileProvider.getProfile();
            console.log('loadProfile', result);
            if (result.status == 200) {

                const profile = result.data;
                console.log('profile nyaaa', profile);
                this.setState({profileData: {
                    name: profile.customer?.outlet?.name,
                    address: profile.customer?.outlet?.address,
                    siup_number: profile.customer?.outlet?.siup_number?.replace(/\bnull\b/,""),
                    type: profile.customer?.outlet?.type,
                    scale: profile.customer?.outlet?.scale,
                    phone_no_1: profile.customer?.outlet?.phone_no_1,
                    phone_no_2: profile.customer?.outlet?.phone_no_2,
                    postal_code: profile.customer?.outlet?.postal_code,
                    meta_lat: profile.customer?.outlet?.meta?.lat || -6.2614927,
                    meta_long: profile.customer?.outlet?.meta?.long || 106.8105998,
                    outlet_brands: profile.customer?.outlet?.outlet_brands?.replace(/\bnull\b/,""),
                    pic_name: profile.customer?.outlet?.pic.name,
                    pic_address: profile.customer?.outlet?.pic.address,
                    pic_postal_code: profile.customer?.outlet?.pic.postal_code,
                    pic_phone_no: profile.customer?.outlet?.pic.phone_no,
                    pic_id_number: profile.customer?.outlet?.pic.id_number,
                    outletId: profile.customer?.outlet?.id,
                    locationSet: (profile.customer?.outlet?.meta?.lat)? true: false,
                    loaded: true
                }});

                let selVillageOutlet = null;
                let selVillagePIC = null;

                
                let selOutletType = profile.customer.outlet?.type;
                let selOutletTypeText = profile.customer.outlet?.type_text;
                let selOutletScale = profile.customer.outlet?.scale;
                let selOutletScaleText = profile.customer.outlet?.scale_text;

                let selIdType = profile.customer.outlet?.pic?.id_type;
                let selIdTypeText = profile.customer.outlet?.pic?.id_type_text;

                if (profile.customer.outlet?.village) {
                    selVillageOutlet = profile.customer.outlet.village;


                    if (profile.customer.outlet?.pic?.village) {
                        selVillagePIC = profile.customer.outlet.pic.village;
                    }
                } 


                this.setState({
                    selVillageOutlet: selVillageOutlet,

                    selVillagePIC: selVillagePIC,

                    selOutletType : selOutletType ,
                    selOutletTypeText : selOutletTypeText,
                    selOutletScale : selOutletScale,
                    selOutletScaleText : selOutletScaleText,
                    selIdType :selIdType,
                    selIdTypeText :selIdTypeText ,

                    loading: false
                })
                this.resizeImage(profile.customer?.outlet?.image_1, 1);
                this.resizeImage(profile.customer?.outlet?.image_2, 2);
            } else if (result.status == 401) {
                this.setState({loading: false});
                await this.context.logout();
                this.props.navigation.navigate('LoggedOutStack', {screen: 'LoggedOut'});
            } else {
                this.setState({loading: false});
            }
        });
    }

    _onScroll = (e) => {
        //infinit scroll
        if (!this.state.enableScroll) return;
        let scrollHeight = e.nativeEvent.contentOffset.y + e.nativeEvent.layoutMeasurement.height;
        if(scrollHeight >= e.nativeEvent.contentSize.height-10){
            if(this.state.loadingSearch || this.state.noMoreLoad) return false;
            this.setState({loadingSearch: true, pageSearch: this.state.pageSearch+1}, () => {
                console.log('Loading scroll', this.state.pageSearch);
                this.populateVillages();
            });

        }
    }

    submitSearch() {
        this.setState({currentLocationData: null, errorSearch: null, pageSearch: 1, loadingSearch: true, noMoreLoad: false, enableScroll: false}, async() => {
            if (this.state.searchVillage.length<3) {
                this.setState({errorSearch: 'Kata kunci minimal 3 karakter', loadingSearch: false});
                return;
            }
            this.populateVillages();
        })

    }

    startSearchTimer(text) {
        this.setState({searchVillage: text});
        if (this.clockTimer) clearInterval(this.clockTimer);
        this.setState({searchTimer: 1}, () => {
            this.clockTimer = setInterval(() => {
                this.setState({ searchTimer: this.state.searchTimer-1 }, () => {
                    if(this.state.searchTimer <= 0) {
                        clearInterval(this.clockTimer);
                        this.searchChange();
                    }
                });
            }, 250);
        })
    }

    searchChange() {
        if (this.state.searchVillage.length<3) {
            this.setState({currentLocationData: null, errorSearch: 'Kata kunci minimal 3 karakter', loadingSearch: false, noMoreLoad: true, enableScroll: false});
            return;
        }
        this.setState({currentLocationData: null, errorSearch: null, pageSearch: 1, loadingSearch: true, noMoreLoad: false, enableScroll: false}, async() => {
            this.populateVillages();
        })

    }

    populateVillages() {
        this.setState({loadingSearch: true}, async() => {
            const result = await profileProvider.getVillages(this.state.searchVillage, this.state.pageSearch, 50);
            console.log('load village', result);
            if (result.status == 200) {
                var listVillage;
                let errorSearch = null
                if (this.state.pageSearch == 1) {
                    listVillage = result.data.results;
                    if (listVillage.length == 0) errorSearch = "Nama Kelurahan tidak ditemukan pada pencarian ini"
                } else {
                    listVillage = (this.state.currentLocationData || []).concat(result.data.results);
                }

                let noMoreLoad = false;
                if (result.data.results.length < 50) noMoreLoad = true; 
                
                this.setState({currentLocationData: listVillage, errorSearch: errorSearch, noMoreLoad: noMoreLoad, loadingSearch: false, enableScroll: true});
            } else {
                // this.setState({errorLoad: true, refreshing: false, loading: false, noMoreLoad: true});
            }
        })
    }

    resizeImage(image, index) {
        if (!image) return;
        let dirty = true;
        if (!image.uri) {
            image = {uri: image};
            dirty = false;
            if (index == 1) {
                this.setState({exist_image_1: true});
            } else {
                this.setState({exist_image_2: true});
            }
        }

        Image.getSize(image.uri, (width, height) => {
            console.log('image size', width, height);
            if (height>width) {
                let scale = height/200;
                let _width = width/scale;
                if (index == 1) {
                    this.setState({image_1: image, imageHeight_1: 200, imageWidth_1: _width, image_1_dirty: dirty, remove_image_1: false});
                } else {
                    this.setState({image_2: image, imageHeight_2: 200, imageWidth_2: _width, image_2_dirty: dirty, remove_image_2: false});
                }
            } else {
                const screenWidth = Dimensions.get('window').width;
                const frameWidth = ((screenWidth - 50) /2) - 10;
                let scale = width/frameWidth;
                let _height = height/scale;
                if (index == 1) {
                    this.setState({image_1: image, imageHeight_1: _height, imageWidth_1: frameWidth, image_1_dirty: dirty});
                } else {
                    this.setState({image_2: image, imageHeight_2: _height, imageWidth_2: frameWidth, image_2_dirty: dirty});
                }
            }    

        });
    }

    pickImage(index) {
        const options = {
            noData: true,
        }
        ImagePicker.launchImageLibrary(options, response => {
            if (response.didCancel) return;
            if (response.uri) {
                this.resizeImage(response, index);
            } else {
                const message = 
                (<Text style={{ color: 'red', flexWrap: 'wrap', fontSize: 14, textAlign: 'left'}} > {response}</Text>);
                this.setState({loading: false, errorModal: true, errorMessage: message});
                return;
            }
        })
    }

    searchLocation(tag) {
        console.log('searchLocation', tag);
        this.setState({
            currentLocationSearch: tag,
            currentLocationData: null,
            viewLocationModal: true
        }, () => this._searchInput.focus() )
    }

    async setLocation(item) {
        switch (this.state.currentLocationSearch) {
            case 'villageOutlet':
                const meta_lat = (this.state.profileData.locationSet) ? this.state.profileData.meta_lat : item.meta.lat;
                const meta_long = (this.state.profileData.locationSet) ? this.state.profileData.meta_long : item.meta.long;
                this.setState({selVillageOutlet: item, viewLocationModal: false, profileData: {...this.state.profileData, meta_lat: meta_lat, meta_long: meta_long } });
                break;
            case 'villagePIC':
                this.setState({selVillagePIC: item, viewLocationModal: false});
                break;

        }
    }


    checkNulldata() {
        this.setState({errorMessage: null});
        let errMessage = {};
        let currentTab = null;
        if (this.state.profileData.name == null || this.state.profileData.name == '') {
            errMessage.name = "Nama Toko tidak boleh kosong";            
            currentTab = 'outlet';
        } else if (this.state.profileData.name.length < 3) {
            errMessage.name = "Nama Toko paling sedikit 3 karakter";            
            currentTab = 'outlet';
        } else {
            delete errMessage.name;
        }   

        if (this.state.profileData.address == null || this.state.profileData.address == '') {
            errMessage.address = "Alamat Toko tidak boleh kosong";            
            currentTab = 'outlet';
        } else if (this.state.profileData.address.length<8){
            errMessage.address = "Alamat Toko minimal 8 karakter";            
            currentTab = 'outlet';
        } else {
            delete errMessage.address;
        }

        if (this.state.selVillageOutlet == null ) {
            errMessage.selVillageOutlet = "Alamat Toko harus lengkap";            
            currentTab = 'outlet';
        } else {
            delete errMessage.selVillageOutlet;
        }

        if (this.state.profileData.postal_code == null || this.state.profileData.postal_code == '') {
            errMessage.postal_code = "Kode Pos tidak boleh kosong";            
            currentTab = 'outlet';
        } else {
            const _format =  /\b\d{5}\b/;
            if (!this.state.profileData.postal_code.match(_format)) {
                errMessage.postal_code = "Kode Pos harus terdiri dari 5 angka";            
                currentTab = 'outlet';
            } else {
                delete errMessage.postal_code;
            }
        }

        if (this.state.selOutletType == null || this.state.selOutletType == 0 ) {
            errMessage.selOutletType = "Tipe Toko harus diisi";            
            currentTab = 'outlet';
        } else {
            delete errMessage.selOutletType;
        }

        if (this.state.selOutletScale == null || this.state.selOutletScale == 0 ) {
            errMessage.selOutletScale = "Skala Toko harus diisi";            
            currentTab = 'outlet';
        } else {
            delete errMessage.selOutletScale;
        }

        if (this.state.profileData.phone_no_1 == null || this.state.profileData.phone_no_1 == '') {
            errMessage.phone_no_1 = "Nomor Telepon tidak boleh kosong";            
            currentTab = 'outlet';
        } else if (this.state.profileData.phone_no_1.length<5){
            errMessage.phone_no_1 = "Nomor Telepon minimal 5 angka";            
            currentTab = 'outlet';
        } else {
            delete errMessage.phone_no_1;
        }
        
        

        if (this.state.profileData.pic_name == null || this.state.profileData.pic_name == '') {
            errMessage.pic_name = "Nama PIC tidak boleh kosong";            
            if (!currentTab) currentTab = 'pic';
        } else if (this.state.profileData.pic_name.length < 3) {
            errMessage.pic_name = "Nama PIC paling sedikit 3 karakter";            
            if (!currentTab) currentTab = 'pic';
        } else {
            delete errMessage.pic_name;
        }

        
        if (this.state.profileData.pic_address == null || this.state.profileData.pic_address == '') {
            errMessage.pic_address = "Alamat PIC tidak boleh kosong";            
            if (!currentTab) currentTab = 'pic';
        } else {
            delete errMessage.pic_address;
        }

        if (this.state.selVillagePIC == null ) {
            errMessage.selVillagePIC = "Alamat PIC harus lengkap";            
            if (!currentTab) currentTab = 'pic';
        } else {
            delete errMessage.selVillagePIC;
        }

        if (this.state.profileData.pic_postal_code == null || this.state.profileData.pic_postal_code == '') {
            errMessage.pic_postal_code = "Kode Pos tidak boleh kosong";            
            if (!currentTab) currentTab = 'pic';
        } else {
            const _format =  /\b\d{5}\b/;
            if (!this.state.profileData.pic_postal_code.match(_format)) {
                errMessage.pic_postal_code = "Kode Pos harus terdiri dari 5 angka";            
                if (!currentTab) currentTab = 'pic';
            } else {
                delete errMessage.pic_postal_code;
            }
        }

        if (this.state.selIdType == null || this.state.selIdType == 0 ) {
            errMessage.selIdType = "Jenis Identitas harus diisi";            
            if (!currentTab) currentTab = 'pic';
        } else {
            delete errMessage.selVillagePIC;
        }

        if (this.state.profileData.pic_id_number == null || this.state.profileData.pic_id_number == '') {
            errMessage.pic_id_number = "Nomor Identitas tidak boleh kosong";            
            if (!currentTab) currentTab = 'pic';
        } else {
            delete errMessage.pic_id_number;
        }

        if (this.state.profileData.pic_phone_no == null || this.state.profileData.pic_phone_no == '') {
            errMessage.pic_phone_no = "Nomor HP tidak boleh kosong";            
            if (!currentTab) currentTab = 'pic';
        } else if (this.state.profileData.pic_phone_no.length<10){
            errMessage.pic_phone_no = "Nomor HP minimal 10 angka";            
            if (!currentTab) currentTab = 'pic';
        } else {
            delete errMessage.pic_phone_no;
        }


        if (Object.keys(errMessage).length > 0) {
            const _fieldError = '_' + Object.keys(errMessage)[0];
            console.log('fieldError', _fieldError);
            this.setState({currentTab: currentTab, errorMessage: errMessage}, () => {
                this[_fieldError].focus();
            });

            return false;
        }

        return true;
    }

    async submitData() {
        if (!this.checkNulldata() ) return;
        this.setState({loading: true}, async() => {
            let formData = new FormData();
            formData.append('name', this.state.profileData.name);
            formData.append('address', this.state.profileData.address);
            if (this.state.profileData.siup_number && this.state.profileData.siup_number != '' ) formData.append('siup_number', this.state.profileData.siup_number);
            formData.append('type', this.state.selOutletType);
            formData.append('scale', this.state.selOutletScale);
            formData.append('phone_no_1', this.state.profileData.phone_no_1);
            formData.append('village', this.state.selVillageOutlet.id);
            formData.append('postal_code', this.state.profileData.postal_code);
            formData.append('meta_lat', this.state.profileData.meta_lat);
            formData.append('meta_long', this.state.profileData.meta_long);
            if (this.state.profileData.outlet_brands && this.state.profileData.outlet_brands != '' ) formData.append('outlet_brands', this.state.profileData.outlet_brands);
            formData.append('pic_name', this.state.profileData.pic_name);
            formData.append('pic_address', this.state.profileData.pic_address);
            formData.append('pic_village', this.state.selVillagePIC.id);
            formData.append('pic_postal_code', this.state.profileData.pic_postal_code);
            formData.append('pic_phone_no', this.state.profileData.pic_phone_no);
            formData.append('pic_id_type', this.state.selIdType);
            formData.append('pic_id_number', this.state.profileData.pic_id_number);
            if (this.state.image_1 && this.state.image_1_dirty) {
                let image_1 = {
                    uri:  this.state.image_1.uri,
                    type: this.state.image_1.type,
                    name: this.state.image_1.fileName
                };
                formData.append('image_1', image_1)
            };
            if (this.state.image_2 && this.state.image_2_dirty) {
                let image_2 = {
                    uri:  this.state.image_2.uri,
                    type: this.state.image_2.type,
                    name: this.state.image_2.fileName
                };

                formData.append('image_2', image_2)
            };

            if (this.state.remove_image_1) {
                formData.append('remove_image_1', 1);
            }

            if (this.state.remove_image_2) {
                formData.append('remove_image_2', 1);
            }


            const result = await profileProvider.updateCustomer(this.state.profileData.outletId, formData);
            console.log('post updateCustomer', result);
            if (result.status == 200 ) {
                this.setState({loading: false, successModal: true});
            } else if (result.status == 401) {
            } else if (result.status == 413) {
                const message = 
                    (<Text style={{ color: 'red', flexWrap: 'wrap', fontSize: 14, textAlign: 'left'}} >  Ukuran file terlalu besar</Text>);
                    this.setState({loading: false, errorModal: true, errorMessage: message});

            } else {
                if (result.data?.data) {
                    const error =result.data.data;
                    const message = Object.keys(error).map((item,index) => {
                        return (
                            <View key={`view-error-${index}`}>
                                {error[item].map((value, key) => {
                                    return (
                                        <Text style={{ color: 'red', flexWrap: 'wrap', fontSize: 14, textAlign: 'left'}} key={`error-${key}`}>  {value}</Text>
                                    )

                                })}
                            </View>
                        )
                    })
                    this.setState({loading: false, errorModal: true, errorMessage: message});
                    
                } else {
                    const message = 
                        <View >
                            <Text style={{ color: 'red', flexWrap: 'wrap', fontSize: 14, textAlign: 'left'}} >  Error tidak dikenal</Text>
                        </View>
                    this.setState({loading: false, errorModal: true, errorMessage: message});

                }
            }

        });
    }

    doDeleteImage() {
        if (this.state.deleteImage == 1) {
            const remove_image = this.state.exist_image_1;
            this.setState({image_1: null, remove_image_1: remove_image, deleteImage: null});
        } else {
            const remove_image = this.state.exist_image_2;
            this.setState({image_2: null, remove_image_2: remove_image, deleteImage: null});
        }
    }

    gotoProfileMenu() {
        this.setState({successModal: false});
        this.props.navigation.navigate('ProfileStack', { screen: 'Profile', params: {} });

    }

    render() {
        const { currentTab, loading, errorModal, errorMessage, successModal, imageHeight_1, imageWidth_1, imageHeight_2, imageWidth_2,
                viewLocationModal, currentLocationSearch, currentLocationData, deleteImage,
                selVillageOutlet, selVillagePIC, searchVillage, loadingSearch, errorSearch,
                image_1, image_2, viewIdTypeModal, selIdType, selIdTypeText, profileData,
                viewOutletTypeModal, selOutletType, selOutletTypeText, viewOutletScaleModal, selOutletScale, selOutletScaleText, _fieldError } = this.state;

        return (
            <Container >
                { !viewLocationModal && <TopHeader parent={this} iconClick={this.iconClick} title="Atur Toko" /> }
                <Loader loading={loading} />
                { !viewLocationModal && 
                    <Content style={{paddingHorizontal: 20}}>
                        <View style={{paddingTop: 10, height: 80, flexDirection: 'column', justifyContent: 'space-between'}}>

                            <View style={{flex: 1, height: 40, flexDirection: 'row', justifyContent: 'center'}}  >
                                <View style={{height: 40, flexDirection: 'row', justifyContent: 'center', width: 200}}  >
                                    <TouchableOpacity 
                                        style={{height: 40, width: 40, borderRadius: 20, backgroundColor: headColor, alignItems: 'center', flexDirection: 'column', justifyContent: 'center'}} 
                                        onPress={() => this.setState({currentTab: 'outlet'})} >
                                        <Text style={{color: 'white'}}>1</Text>
                                    </TouchableOpacity>
                                    <View style={{height: 40, flex: 1, alignItems: 'center', flexDirection: 'row'}} >
                                        <View style={[{flex: 1, borderStyle: 'dashed', height: 2, borderWidth: 1, borderRadius: 2}, currentTab=='outlet'? {borderColor: greyColor}: {borderColor: headColor} ]}></View>
                                    </View>
                                    <TouchableOpacity 
                                        style={[{height: 40, width: 40, borderRadius: 20, alignItems: 'center', flexDirection: 'column', justifyContent: 'center' }, currentTab=='outlet'? {backgroundColor: greyColor}: {backgroundColor: headColor}]} 
                                        onPress={() => this.setState({currentTab: 'pic'})} >
                                        <Text style={{color: 'white'}}>2</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}  >
                                <View style={{flexDirection: 'row', justifyContent: 'space-between', width: 240}}  >
                                    <View style={{width: 80, alignItems: 'center', flexDirection: 'column', justifyContent: 'center'}} >
                                        <Text style={{fontSize: 14, color: greyColor}}>Data Toko</Text>
                                    </View>
                                    
                                    <View style={{width: 80, alignItems: 'center', flexDirection: 'column', justifyContent: 'center' }} >
                                        <Text style={{fontSize: 14, color: greyColor}}>Data PIC</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                        { currentTab == 'outlet' &&
                            <View style={{marginTop: 0}}>
                                <View>
                                    <Label style={[style.formLabel]}>Nama Toko</Label>
                                    <Item regular style={style.roundedInput}>
                                        <TextInput ref={r => this._name =  r} style={[style.formInput]} value={profileData.name} onChangeText={(text) => this.setState({profileData: {...profileData, name: text}})} />
                                    </Item>
                                    {errorMessage?.name && <Text style={[style.fieldError]}>{errorMessage.name}</Text> }
                                </View>
                                <View>
                                    <Label style={[style.formLabel]}>Alamat Toko</Label>
                                    <Item regular style={style.roundedInput}>
                                        <TextInput  ref={r => this._address =  r}
                                            style={[style.formInput, {textAlignVertical: 'top'}]} multiline = {true} numberOfLines = {4} value={profileData.address} onChangeText={(text) => this.setState({profileData: {...profileData, address: text}})}/>
                                    </Item>
                                    {errorMessage?.address && <Text style={[style.fieldError]}>{errorMessage.address}</Text> }
                                </View>
                                <View style={{flex: 1, flexDirection: 'column', justifyContent: 'space-between'}}>
                                    <View style={{flex: 1, paddingLeft: 5}}>
                                        <Label style={[style.formLabel]}>Kelurahan</Label>
                                        <Item regular style={style.roundedInput} onPress={() => this.searchLocation('villageOutlet')}>
                                            <TextInput ref={r => this._selVillageOutlet =  r} 
                                            style={[style.formInput, {textAlignVertical: 'top'}]} multiline = {true} numberOfLines = {3} value={selVillageOutlet?.name} editable={false} />
                                        </Item>
                                        {errorMessage?.selVillageOutlet && <Text style={[style.fieldError]}>{errorMessage.selVillageOutlet}</Text> }
                                    </View>
                                </View>
                                <View>
                                    <Label style={[style.formLabel]}>Kode Pos</Label>
                                    <Item regular style={style.roundedInput}>
                                        <TextInput keyboardType='numeric' ref={r => this._postal_code =  r} style={[style.formInput]} value={profileData.postal_code} onChangeText={(text) => this.setState({profileData: {...profileData, postal_code: text}})} />
                                    </Item>
                                    {errorMessage?.postal_code && <Text style={[style.fieldError]}>{errorMessage.postal_code}</Text> }
                                </View>
                                {profileData.loaded &&
                                    <View style={{marginBottom: 24}}>
                                        <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between', height: 200}}>
                                            <MapView
                                               style={{ flex: 1, width: '100%' }} //window pake Dimensions
                                               region={{
                                                  latitude: parseFloat( profileData.meta_lat),
                                                  longitude: parseFloat(profileData.meta_long),
                                                  latitudeDelta: 0.0922,
                                                  longitudeDelta: 0.0421 
                                               }} >
                                            <MapView.Marker
                                               coordinate={{
                                                  latitude:  parseFloat( profileData.meta_lat),
                                                  longitude: parseFloat( profileData.meta_long),
                                               }}
                                               title="Lokasi"
                                               description="Hello" />
                                            </MapView>
                                        </View>
                                        <View style={{flex: 1}}>
                                            <Button style={[style.buttonWhite, {borderTopRadius: 0}]}  full bordered onPress={() => this.requestLocation() }>
                                                <Text style={[style.textButtonWhite, {textTransform: 'capitalize'}]}>Gunakan lokasi saat ini</Text>
                                            </Button>
                                        </View>
                                    </View>

                                }

                                <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between', marginBottom: 24}}>
                                    <View style={{flex: 1, paddingRight: 5}}>
                                        <Label style={[style.formLabel]}>Upload Foto</Label>
                                        <TouchableOpacity style={[{height: 220, 
                                                flexDirection: 'row', 
                                                justifyContent: 'center', 
                                                alignItems: 'center', 
                                                backgroundColor: 'rgb(220, 220, 220)', 
                                                borderStyle: 'dashed', 
                                                borderColor: 'rgb(138, 138, 138)', 
                                                borderWidth: 1, 
                                                borderRadius: 8}]} 
                                                padder={true}
                                                onPress={() => this.pickImage(1)}
                                                >
                                            { image_1 ?
                                                <Image style={{resizeMode: 'center', borderWidth: 1, width: imageWidth_1, height: imageHeight_1}} source={{uri: image_1.uri}} />
                                            :
                                                <MaterialCommunityIcons name="upload" size={14} />
                                            }
                                        </TouchableOpacity>
                                        { image_1 &&
                                            <Button bordered style={[style.buttonWhite, {position: 'absolute', bottom: 5, right: 10, height: 24, borderRadius: 4}]}
                                                onPress={() => this.setState({deleteImage: 1})} >
                                                <MaterialCommunityIcons name="trash-can-outline" color={'red'} size={20}   />
                                            </Button>
                                        }
                                        {errorMessage?.image && <Text style={[style.fieldError]}>{errorMessage.image}</Text> }

                                    </View>
                                    <View style={{flex: 1, paddingLeft: 5}}>
                                        <Label style={[style.formLabel]}></Label>
                                        <TouchableOpacity style={[{height: 220, 
                                                flexDirection: 'row', 
                                                justifyContent: 'center', 
                                                alignItems: 'center', 
                                                backgroundColor: 'rgb(220, 220, 220)', 
                                                borderStyle: 'dashed', 
                                                borderColor: 'rgb(138, 138, 138)', 
                                                borderWidth: 1, 
                                                borderRadius: 8}]} 
                                                padder={true}
                                                onPress={() => this.pickImage(2)}
                                                >
                                            { image_2 ?
                                                <Image style={{resizeMode: 'center', borderWidth: 1, width: imageWidth_2, height: imageHeight_2}} source={{uri: image_2.uri}} />

                                            :
                                                <MaterialCommunityIcons name="upload" size={14} />
                                            }
                                        </TouchableOpacity>
                                        { image_2 &&
                                            <Button bordered style={[style.buttonWhite, {position: 'absolute', bottom: 5, right: 5, height: 24, borderRadius: 4}]} 
                                                onPress={() => this.setState({deleteImage: 2})} >
                                                <MaterialCommunityIcons name="trash-can-outline" color={'red'} size={20}   />
                                            </Button>
                                        }
                                    </View>
                                </View>

                                <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between'}}>
                                    <View style={{flex: 1, paddingRight: 5}}>
                                        <Label style={[style.formLabel]}>Tipe Toko</Label>
                                        <Item regular style={style.roundedInput}  onPress={() => this.setState({viewOutletTypeModal: true})}>
                                            <TextInput ref={r => this._selOutletType =  r} style={[style.formInput]} value={selOutletTypeText} editable={false} />
                                        </Item>
                                        {errorMessage?.selOutletType && <Text style={[style.fieldError]}>{errorMessage.selOutletType}</Text> }
                                    </View>
                                    <View style={{flex: 1, paddingLeft: 5}}>
                                        <Label style={[style.formLabel]}>Skala Toko</Label>
                                        <Item regular style={style.roundedInput}  onPress={() => this.setState({viewOutletScaleModal: true})}>
                                            <TextInput ref={r => this._selOutletScale =  r} style={[style.formInput]} value={selOutletScaleText} editable={false} />
                                        </Item>
                                        {errorMessage?.selOutletScale && <Text style={[style.fieldError]}>{errorMessage.selOutletScale}</Text> }
                                    </View>
                                </View>

                                <View >
                                    <Label style={[style.formLabel]}>Brand Produk yang Dijual</Label>
                                    <Item regular style={style.roundedInput}  >
                                        <TextInput style={[style.formInput]} value={profileData.outlet_brands} onChangeText={(text) => this.setState({profileData: {...profileData, outlet_brands: text}})} />
                                    </Item>
                                    <Text style={[style.fieldError, {color: greyColor}]}>** Anda bisa memasukkan lebih dari 1 brand dengan memisahkan menggunakan tanda koma (,)</Text>
                                </View>

                                <View>
                                    <Label style={[style.formLabel]}>Nomor SIUP</Label>
                                    <Item regular style={style.roundedInput}>
                                        <TextInput style={[style.formInput]} value={profileData.siup_number} onChangeText={(text) => this.setState({profileData: {...profileData, siup_number: text}})} />
                                    </Item>
                                </View>

                                <View>
                                    <Label style={[style.formLabel]}>Nomor Telepon</Label>
                                    <Item regular style={style.roundedInput}>
                                        <TextInput keyboardType='numeric' ref={r => this._phone_no_1 =  r} style={[style.formInput]} value={profileData.phone_no_1} onChangeText={(text) => this.setState({profileData: {...profileData, phone_no_1: text}})} />
                                    </Item>
                                    {errorMessage?.phone_no_1 && <Text style={[style.fieldError]}>{errorMessage.phone_no_1}</Text> }
                                </View>

                            </View>
                        }

                        { currentTab == 'pic' &&
                            <View style={{marginTop: 0}}>
                                <View>
                                    <Label style={[style.formLabel]}>Nama PIC</Label>
                                    <Item regular style={style.roundedInput}>
                                        <TextInput ref={r => this._pic_name =  r} style={[style.formInput]} value={profileData.pic_name} onChangeText={(text) => this.setState({profileData: {...profileData, pic_name: text}})} />
                                    </Item>
                                    {errorMessage?.pic_name && <Text style={[style.fieldError]}>{errorMessage.pic_name}</Text> }
                                </View>
                                <View>
                                    <Label style={[style.formLabel]}>Alamat PIC</Label>
                                    <Item regular style={style.roundedInput}>
                                        <TextInput ref={r => this._pic_address =  r} style={[style.formInput, {textAlignVertical: 'top'}]}  multiline = {true} numberOfLines = {4} value={profileData.pic_address} onChangeText={(text) => this.setState({profileData: {...profileData, pic_address: text}})} />
                                    </Item>
                                    {errorMessage?.pic_address && <Text style={[style.fieldError]}>{errorMessage.pic_address}</Text> }
                                </View>
                                <View style={{flex: 1, flexDirection: 'column', justifyContent: 'space-between'}}>
                                    <View style={{flex: 1, paddingLeft: 5}}>
                                        <Label style={[style.formLabel]}>Kelurahan</Label>
                                        <Item regular style={style.roundedInput} onPress={() => this.searchLocation('villagePIC')}>
                                            <TextInput ref={r => this._selVillagePIC =  r} 
                                            style={[style.formInput, {textAlignVertical: 'top'}]} multiline = {true} numberOfLines = {3} value={selVillagePIC?.name} editable={false} />
                                        </Item>
                                        {errorMessage?.selVillagePIC && <Text style={[style.fieldError]}>{errorMessage.selVillagePIC}</Text> }
                                    </View>

                                </View>
                                <View>
                                    <Label style={[style.formLabel]}>Kode Pos</Label>
                                    <Item regular style={style.roundedInput}>
                                        <TextInput keyboardType='numeric' ref={r => this._pic_postal_code =  r} style={[style.formInput]} value={profileData.pic_postal_code} onChangeText={(text) => this.setState({profileData: {...profileData, pic_postal_code: text}})} />
                                    </Item>
                                    {errorMessage?.pic_postal_code && <Text style={[style.fieldError]}>{errorMessage.pic_postal_code}</Text> }
                                </View>
                                <View>
                                    <Label style={[style.formLabel]}>Jenis Identitas</Label>
                                    <Item regular style={style.roundedInput}  onPress={() => this.setState({viewIdTypeModal: true})}>
                                        <TextInput ref={r => this._selIdType =  r} style={[style.formInput]} value={selIdTypeText} editable={false} />
                                    </Item>
                                    {errorMessage?.selIdType && <Text style={[style.fieldError]}>{errorMessage.selIdType}</Text> }
                                </View>
                                <View>
                                    <Label style={[style.formLabel]}>Nomor Identitas</Label>
                                    <Item regular style={style.roundedInput}>
                                        <TextInput keyboardType='numeric' ref={r => this._pic_id_number =  r} style={[style.formInput]} value={profileData.pic_id_number} onChangeText={(text) => this.setState({profileData: {...profileData, pic_id_number: text}})} />
                                    </Item>
                                    {errorMessage?.pic_id_number && <Text style={[style.fieldError]}>{errorMessage.pic_id_number}</Text> }
                                </View>
                                <View>
                                    <Label style={[style.formLabel]}>Nomor HP</Label>
                                    <Item regular style={style.roundedInput}>
                                        <TextInput keyboardType='numeric' ref={r => this._pic_phone_no =  r} style={[style.formInput]} value={profileData.pic_phone_no} onChangeText={(text) => this.setState({profileData: {...profileData, pic_phone_no: text}})} />
                                    </Item>
                                    {errorMessage?.pic_phone_no && <Text style={[style.fieldError]}>{errorMessage.pic_phone_no}</Text> }
                                </View>
                            </View>
                        }


                    </Content>
                }
                { viewLocationModal && <TopHeader parent={this} noBackButton={true} title="Pilih Kelurahan" /> }
                { viewLocationModal && 
                    <View style={[{ flexDirection: 'column', flex: 1, paddingHorizontal: 20, paddingVertical: 10, justifyContent: 'space-between' }]} >
                        <View style={{height: 50, paddingHorizontal: 10, borderRadius: 4, borderColor: greyColor, borderWidth: 0.5, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center'}} >
                            <Icon name='search' style={{color: greyColor}} />
                            <TextInput 
                                placeholder='Cari nama Kelurahan disini' 
                                returnKeyType="search" 
                                ref={r => this._searchInput = r }
                                style={{flex: 1}}
                                onChangeText={(text) => this.startSearchTimer(text)} 
                                value={searchVillage}
                                onSubmitEditing={()=>{this.submitSearch() }}

                            />
                            { searchVillage.length>0 &&
                                <Icon name='close' style={{color: greyColor}} onPress={() => {this.startSearchTimer(""); this._searchInput.focus()} } /> 
                            }

                        </View>
                        {errorSearch && <Text style={[style.fieldError, {flex:1, marginTop: 20, alignSelf: 'flex-start'}]}>{errorSearch}</Text> }
                        { currentLocationData &&
                            <Content style={{flex: 1}} onScroll={this._onScroll} scrollEventThrottle={50} >
                                {currentLocationData.map((item) => {
                                    return(
                                        <TouchableOpacity 
                                            style={{flexDirection: 'row', justifyContent: 'center', paddingLeft: 5, paddingVertical: 5, borderBottomWidth: 0.5, borderBottomColor: greyColor}} key={`location-${item.id}`}
                                            onPress={() => this.setLocation(item)} >
                                            <Text style={[pageStyle.titleText,{flex: 1, fontSize: 14}]}  numberOfLines = {1} ellipsizeMode='tail'>{item.name}</Text>
                                            <MaterialCommunityIcons name="chevron-right" color={greyColor} size={26} />

                                        </TouchableOpacity>
                                    )
                                })}
                            </Content>
                        }
                        {loadingSearch && <Spinner color='#03a9f4' />}
                        <View style={{flexDirection: 'row', alignItems: 'flex-end', paddingTop: 20}}>
                            <View style={{flex: 1}}>
                                <Button style={style.buttonBrown} full onPress={() =>  {this.setState({viewLocationModal: false})} }>
                                    <Text style={{textTransform: 'capitalize'}}>Batal</Text>
                                </Button>
                            </View>
                        </View>
                    </View>
                }
                
                <Modal 
                    isVisible={viewIdTypeModal} 
                    coverScreen={true} 
                    onBackdropPress={() => this.setState({viewIdTypeModal: false})}
                    backdropOpacity={0.7} 
                    style={{flexDirection: 'row', alignItems: 'flex-end', flex: 1, margin: 0 }}
                    >
                    <View style={[style.whiteBox, style.modalBox, { height: 400, flexDirection: 'column', flex: 1, padding: 20 }]} >
                        <Content style={{paddingRight: 20}} >
                            <View >
                                <Text style={style.productTitle} >Pilih Jenis Identitas</Text>
                            </View>
                                <TouchableOpacity 
                                    style={{flexDirection: 'row', justifyContent: 'center', paddingVertical: 5, borderBottomWidth: 0.5, borderBottomColor: greyColor}}
                                    onPress={() => {this.setState({selIdType: '1', selIdTypeText: 'KTP', viewIdTypeModal: false}) }} >
                                    <Text style={[pageStyle.titleText,{flex: 1, fontSize: 14}]}  numberOfLines = {1} ellipsizeMode='tail'>KTP</Text>
                                    <MaterialCommunityIcons name="chevron-right" color={greyColor} size={26} />

                                </TouchableOpacity>
                                <TouchableOpacity 
                                    style={{flexDirection: 'row', justifyContent: 'center', paddingVertical: 5, borderBottomWidth: 0.5, borderBottomColor: greyColor}}
                                    onPress={() => this.setState({selIdType: '2', selIdTypeText: 'SIM', viewIdTypeModal: false})} >
                                    <Text style={[pageStyle.titleText,{flex: 1, fontSize: 14}]}  numberOfLines = {1} ellipsizeMode='tail'>SIM</Text>
                                    <MaterialCommunityIcons name="chevron-right" color={greyColor} size={26} />

                                </TouchableOpacity>
                        </Content>
                        <View style={{flexDirection: 'row', alignItems: 'flex-end', paddingTop: 20}}>
                            <View style={{flex: 1}}>
                                <Button style={style.buttonBrown} full onPress={() =>  {this.setState({viewIdTypeModal: false})} }>
                                    <Text style={{textTransform: 'capitalize'}}>Batal</Text>
                                </Button>
                            </View>
                        </View>
                    </View>
                </Modal>

                <Modal 
                    isVisible={viewOutletTypeModal} 
                    coverScreen={true} 
                    onBackdropPress={() => this.setState({viewOutletTypeModal: false})}
                    backdropOpacity={0.7} 
                    style={{flexDirection: 'row', alignItems: 'flex-end', flex: 1, margin: 0 }}
                    >
                    <View style={[style.whiteBox, style.modalBox, { height: 400, flexDirection: 'column', flex: 1, padding: 20 }]} >
                        <Content style={{paddingRight: 20}} >
                            <View >
                                <Text style={style.productTitle} >Pilih Tipe Toko</Text>
                            </View>
                                <TouchableOpacity 
                                    style={{flexDirection: 'row', justifyContent: 'center', paddingVertical: 5, borderBottomWidth: 0.5, borderBottomColor: greyColor}}
                                    onPress={() => {this.setState({selOutletType: '1', selOutletTypeText: 'Bengkel', viewOutletTypeModal: false}) }} >
                                    <Text style={[pageStyle.titleText,{flex: 1, fontSize: 14}]}  numberOfLines = {1} ellipsizeMode='tail'>Bengkel</Text>
                                    <MaterialCommunityIcons name="chevron-right" color={greyColor} size={26} />

                                </TouchableOpacity>
                                <TouchableOpacity 
                                    style={{flexDirection: 'row', justifyContent: 'center', paddingVertical: 5, borderBottomWidth: 0.5, borderBottomColor: greyColor}}
                                    onPress={() => this.setState({selOutletType: '2', selOutletTypeText: 'Toko', viewOutletTypeModal: false})} >
                                    <Text style={[pageStyle.titleText,{flex: 1, fontSize: 14}]}  numberOfLines = {1} ellipsizeMode='tail'>Toko</Text>
                                    <MaterialCommunityIcons name="chevron-right" color={greyColor} size={26} />

                                </TouchableOpacity>
                                <TouchableOpacity 
                                    style={{flexDirection: 'row', justifyContent: 'center', paddingVertical: 5, borderBottomWidth: 0.5, borderBottomColor: greyColor}}
                                    onPress={() => this.setState({selOutletType: '3', selOutletTypeText: 'Toko & Bengkel', viewOutletTypeModal: false})} >
                                    <Text style={[pageStyle.titleText,{flex: 1, fontSize: 14}]}  numberOfLines = {1} ellipsizeMode='tail'>Toko & Bengkel</Text>
                                    <MaterialCommunityIcons name="chevron-right" color={greyColor} size={26} />

                                </TouchableOpacity>
                        </Content>
                        <View style={{flexDirection: 'row', alignItems: 'flex-end', paddingTop: 20}}>
                            <View style={{flex: 1}}>
                                <Button style={style.buttonBrown} full onPress={() =>  {this.setState({viewOutletTypeModal: false})} }>
                                    <Text style={{textTransform: 'capitalize'}}>Batal</Text>
                                </Button>
                            </View>
                        </View>
                    </View>
                </Modal>

                <Modal 
                    isVisible={viewOutletScaleModal} 
                    coverScreen={true} 
                    onBackdropPress={() => this.setState({viewOutletScaleModal: false})}
                    backdropOpacity={0.7} 
                    style={{flexDirection: 'row', alignItems: 'flex-end', flex: 1, margin: 0 }}
                    >
                    <View style={[style.whiteBox, style.modalBox, { height: 400, flexDirection: 'column', flex: 1, padding: 20 }]} >
                        <Content style={{paddingRight: 20}} >
                            <View >
                                <Text style={style.productTitle} >Pilih Skala Toko</Text>
                            </View>
                                <TouchableOpacity 
                                    style={{flexDirection: 'row', justifyContent: 'center', paddingVertical: 5, borderBottomWidth: 0.5, borderBottomColor: greyColor}}
                                    onPress={() => {this.setState({selOutletScale: '1', selOutletScaleText: 'Grosir', viewOutletScaleModal: false}) }} >
                                    <Text style={[pageStyle.titleText,{flex: 1, fontSize: 14}]}  numberOfLines = {1} ellipsizeMode='tail'>Grosir</Text>
                                    <MaterialCommunityIcons name="chevron-right" color={greyColor} size={26} />

                                </TouchableOpacity>
                                <TouchableOpacity 
                                    style={{flexDirection: 'row', justifyContent: 'center', paddingVertical: 5, borderBottomWidth: 0.5, borderBottomColor: greyColor}}
                                    onPress={() => this.setState({selOutletScale: '2', selOutletScaleText: 'Semi Grosir', viewOutletScaleModal: false})} >
                                    <Text style={[pageStyle.titleText,{flex: 1, fontSize: 14}]}  numberOfLines = {1} ellipsizeMode='tail'>Semi Grosir</Text>
                                    <MaterialCommunityIcons name="chevron-right" color={greyColor} size={26} />

                                </TouchableOpacity>
                                <TouchableOpacity 
                                    style={{flexDirection: 'row', justifyContent: 'center', paddingVertical: 5, borderBottomWidth: 0.5, borderBottomColor: greyColor}}
                                    onPress={() => this.setState({selOutletScale: '3', selOutletScaleText: 'Retail', viewOutletScaleModal: false})} >
                                    <Text style={[pageStyle.titleText,{flex: 1, fontSize: 14}]}  numberOfLines = {1} ellipsizeMode='tail'>Retail</Text>
                                    <MaterialCommunityIcons name="chevron-right" color={greyColor} size={26} />

                                </TouchableOpacity>
                        </Content>
                        <View style={{flexDirection: 'row', alignItems: 'flex-end', paddingTop: 20}}>
                            <View style={{flex: 1}}>
                                <Button style={style.buttonBrown} full onPress={() =>  {this.setState({viewOutletScaleModal: false})} }>
                                    <Text style={{textTransform: 'capitalize'}}>Batal</Text>
                                </Button>
                            </View>
                        </View>
                    </View>
                </Modal>


                <Modal 
                    isVisible={successModal} 
                    coverScreen={true} 
                    onBackdropPress={() => this.setState({successModal: false})}
                    backdropOpacity={0.7} 
                    style={{flexDirection: 'row', alignItems: 'flex-end', flex: 1, margin: 0 }}
                    >
                    <View style={[style.whiteBox, {height: 200, flex: 1, paddingTop: 10, paddingBottom: 10, paddingHorizontal: 20}]} >
                        <View style={[{flexDirection: 'row', marginBottom: 5} ]}>
                            <View style={{flex: 1, justifyContent:'center'}}>
                                <Text style={[style.productTitle, {textAlign: 'center', fontSize: 16}]}>Data Berhasil Disimpan</Text>
                                <Text style={[{textAlign: 'center', fontSize: 14, color: greyColor}]}>Kami akan melakukan verifikasi untuk memastikan data kamu benar. </Text>
                            </View>
                        </View>
                        <View style={[{flex: 1, marginTop: 30}]}>
                            <View style={{flexDirection: 'row', alignItems: 'flex-end'}}>
                                <View style={{flex: 1}}>
                                    <Button style={style.buttonBrown} full onPress={() =>  this.gotoProfileMenu() }>
                                        <Text style={{textTransform: 'capitalize'}}>OK</Text>
                                    </Button>
                                </View>
                            </View> 


                        </View>
                    </View>
                </Modal>
                <Modal 
                    isVisible={errorModal} 
                    coverScreen={true} 
                    onBackdropPress={() => this.setState({errorModal: false}) }
                    backdropOpacity={0.7} 
                    style={{flexDirection: 'row', alignItems: 'flex-end', flex: 1, margin: 0 }}
                    >
                    <View style={[style.whiteBox, {height: 200, flex: 1, paddingTop: 10, paddingBottom: 10, paddingHorizontal: 20}]} >
                        <View style={[{flexDirection: 'row', marginBottom: 5} ]}>
                            <View style={{flex: 1, justifyContent:'center'}}>
                                <Text style={[style.productTitle, {textAlign: 'center', fontSize: 16, marginBottom: 10}]}>Gagal</Text>
                            </View>
                        </View>
                        <View style={[{flex: 1, flexDirection: 'column'}]} >
                            <View style={{flex: 1}}>
                                {errorMessage}
                            </View>
                            <View style={{flexDirection: 'row', alignItems: 'flex-end'}}>
                            <View style={{flex: 1}}>
                                <Button style={style.buttonBrown} full onPress={() =>  this.setState({errorModal: false}) }>
                                    <Text style={{textTransform: 'capitalize'}}>OK</Text>
                                </Button>
                            </View> 
                            </View>
                        </View>
                    </View>
                </Modal>

                <Modal 
                isVisible={deleteImage != null} 
                coverScreen={true} 
                onBackdropPress={() => this.setState({deleteImage: null}) }
                backdropOpacity={0.7} 
                style={{flexDirection: 'row', alignItems: 'flex-end', flex: 1, margin: 0 }}
                >
                <View style={[style.whiteBox, {flex: 1, height: 200, flexDirection: 'column', paddingBottom: 10}]} >
                    <View style={[{flexDirection: 'row', marginBottom: 5} ]}>
                        <View style={{flex: 1, justifyContent:'center'}}>
                            <Text style={[style.productTitle, {textAlign: 'center', fontSize: 16} ]}>Hapus Produk</Text>
                        </View>
                    </View>
                    <View style={[{flex: 1, flexDirection: 'column'}]} paddingHorizontal={20}>
                        <View style={{flex: 1}}>
                            <Text style={[{fontSize: 14, color: greyColor, textAlign: 'center'}]} >Apakan Anda yakin menghapus Foto ini?</Text>
                        </View>
                        <View style={{flexDirection: 'row', alignItems: 'flex-end'}}>
                            <View style={{flex: 1, paddingRight: 6}}>
                                <Button style={style.buttonWhite}  full bordered onPress={() =>this.setState({deleteImage: null}) }>
                                    <Text style={[style.textButtonWhite, {textTransform: 'capitalize'}]}>Tidak</Text>
                                </Button>
                            </View>
                            <View style={{flex: 1, paddingLeft: 6}}>
                                <Button style={style.buttonBrown} full onPress={() =>  {this.doDeleteImage()} }>
                                    <Text style={{textTransform: 'capitalize'}}>Ya</Text>
                                </Button>
                            </View>
                        </View> 
                    </View>
                </View>
            </Modal>

            { !viewLocationModal &&
                <Footer style={{backgroundColor: 'white', padding: 10, height: 70}}>
                    { currentTab == 'outlet' ?
                        <View style={{flex: 1}}>
                            <Button style={style.buttonBrown} full onPress={() => this.setState({currentTab: 'pic'}) }>
                                <Text style={{textTransform: 'capitalize'}}>Lanjut</Text>
                            </Button>
                        </View>
                    :
                        <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between'}}>
                            <View style={{flex: 1, paddingRight: 6}}>
                                <Button style={style.buttonWhite}  full bordered onPress={() => this.setState({currentTab: 'outlet'}) }>
                                    <Text style={[style.textButtonWhite, {textTransform: 'capitalize'}]}>Kembali</Text>
                                </Button>
                            </View>
                            <View style={{flex: 1, paddingLeft: 6}}>
                                <Button style={style.buttonBrown} full onPress={() => this.submitData()}>
                                    <Text style={{textTransform: 'capitalize'}}>Simpan</Text>
                                </Button>
                            </View>
                        </View>
                    }
                </Footer>
            }
            </Container>
        );
    }
}


const pageStyle = StyleSheet.create({  
    titleText: {
        fontSize: 11, 
        fontFamily: 'Open Sans',
        textAlign: 'left',
        color: 'rgb(140, 140, 140)',
        alignSelf: 'center'
    },

})  
