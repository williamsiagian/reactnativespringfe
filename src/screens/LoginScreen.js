import React, { Component, useContext  } from 'react';
import {Container, Content, Button, Text, Body, Form, Item, Input, Label, View, CheckBox, Card, CardItem, Left } from 'native-base';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from '@react-native-community/async-storage';

import style from './../../layout/style'

import AuthContext,{ AuthProvider } from "./../../providers/AuthProvider";
import TopHeader from './../../layout/TopHeader';
import Loader from './../../layout/Loader';

export default class LoginScreen extends  React.Component {
    static contextType = AuthContext
    constructor(props) {
        super(props);
        this._password = React.createRef();
        this.state = {
            email : '',
            password: '',
            rememberMeChecked: false,
            errorSubmit: null,
            isEmailValid: null,
            isPasswordValid: null,
            loading: false
        };
        this.loadRemembered = this.loadRemembered.bind(this);

    }

    async loadRemembered() {
        let rememberedLogin = {email: '', password: ''};
        const remembered = await AsyncStorage.getItem("darkopart-sign");
        if (remembered) {
            rememberedLogin = JSON.parse(remembered);
            rememberedLogin.rememberMeChecked = true;
            console.log('loadRemembered', rememberedLogin);
            this.setState(rememberedLogin, () => {
                this.validateEmail(rememberedLogin.email);
                this.validatePassword(rememberedLogin.password);
            });
            
        }
    }

    validateEmail = (text) => {
        // this.setState({errorSubmit: null});
        var errMessage;
        if (text.trim() == "") {
            errMessage = "Email tidak boleh kosong";
            this.setState({ email: text, isEmailValid: false, errorSubmit: {...this.state.errorSubmit, email: errMessage} })
            return false;
        }

        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (reg.test(text) === false) {
            errMessage = "Gunakan format email yang benar";
            this.setState({ email: text, isEmailValid: false, errorSubmit: {...this.state.errorSubmit, email: errMessage} })
            return false;
        } else {
            this.setState({ email: text, isEmailValid: true, errorSubmit: {...this.state.errorSubmit, email: null} })
        }
    }

    validatePassword = (text) => {
        // this.setState({errorSubmit: null});
        var errMessage;
        var isPasswordValid;
        
        if (text.trim() == "") {
            errMessage = "Kata Sandi tidak boleh kosong";
            this.setState({ password: text, isPasswordValid: false, errorSubmit: {...this.state.errorSubmit, password: errMessage} })
            return false;
        }

        const pass_format =  /^(?=.*[0-9])(?=.*[A-Z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,100}$/;
        // const pass_format =  /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,100}$/;
        if (text.match(pass_format)) {
            errMessage = null;
            isPasswordValid = true;
            // this.setState({ password: text, isPasswordValid: true, errorSubmit: {...this.state.errorSubmit, password: null} })
        } else {
            errMessage = "Kata Sandi harus minimal 8 karakter dan terdiri dari minimal 1 huruf besar, huruf kecil, angka, dan special character";
            isPasswordValid = false;
        }


        this.setState({ password: text, 
            isPasswordValid: isPasswordValid, 
            errorSubmit: {...this.state.errorSubmit, password: errMessage} } 
        )
    }


    componentDidMount() {
        this.props.navigation.addListener('focus', this.loadRemembered);
    }

    componentWillUnmount() {}

    async submitData() {
        const {email, password, isEmailValid, isPasswordValid} = this.state;
        let errMessage = {};

        if (email=='') {
            errMessage['email'] = "Email tidak boleh kosong";
        } else if (!isEmailValid) {
            errMessage['email'] = "Gunakan format email yang benar";
        }

        if (password=='') {
            errMessage['password'] = "Kata Sandi tidak boleh kosong";
        } else if (!isPasswordValid) {
            errMessage['password'] = "Kata Sandi harus minimal 8 karakter dan terdiri dari minimal 1 huruf besar, huruf kecil, angka, dan special character";
        }

        if (Object.keys(errMessage).length > 0) {
            this.setState({errorSubmit: errMessage, loading: false});
            return;
        }


        this.setState({errorSubmit: null, loading: true});
        const data = {
            email: this.state.email,
            password: this.state.password
        }

        const result = await this.context.login(data);
        console.log('result login', result);
        if (result.status == 200 ) {
            if (this.state.rememberMeChecked){ 
                AsyncStorage.setItem("darkopart-sign", JSON.stringify(data));
            } else {
                AsyncStorage.removeItem("darkopart-sign");
            } 
            await this.context.getUnreadNotificationTotal();
            await this.context.getUserCartCount();

            const fcm_token = await AsyncStorage.getItem("fcm_token");
            const resToken =await this.context.updateFcmToken(fcm_token);
            console.log('resToken', resToken);
            
            this.props.navigation.navigate('Home');
        } else {
            this.setState({errorSubmit: {"result": result.data.message}, loading: false});
        }
    }

    render() {
        const { email, password, isEmailValid, isPasswordValid, rememberMeChecked, errorSubmit, loading } = this.state;
        return (
            <Container >
                <TopHeader parent={this} iconClick={this.iconClick}  />
                <Loader loading={loading} />
                <Content style={{padding: 20}}>
                    <View>
                        <Text style={[style.heading6, {marginBottom: 16}]} >Masuk</Text>
                    </View>

                    <View style={{flexDirection: 'row', paddingBottom: 30, alignItems: 'flex-end'}}>
                        <Text >Belum punya akun?  </Text>
                        <Text style={[style.link, {fontSize: 14}]} onPress={() => this.props.navigation.navigate('Signup')}>Daftar</Text>
                    </View>
                    <Form>
                        <View>
                            <Label style={style.formLabel}>Email</Label>
                            <Item regular style={style.roundedInput}>
                                <Input value={email} keyboardType="email-address" returnKeyType="next" onSubmitEditing={(event) => this._password._root.focus()}  onChangeText={(text) => this.validateEmail(text)} />
                                {isEmailValid != null &&
                                    <MaterialCommunityIcons name={isEmailValid?'check-circle': 'close-circle'} color={isEmailValid?'blue': 'red'} size={20} style={{paddingRight: 5}} />
                                }
                            </Item>
                            {errorSubmit?.email && <Text style={[style.fieldError]}>{errorSubmit.email}</Text> }
                        </View>
                        <View>
                            <Label style={style.formLabel}>Kata Sandi</Label>
                            <Item regular style={style.roundedInput}>
                                <Input value={password} ref={(c) => this._password = c} secureTextEntry={true}  onChangeText={(text) => this.validatePassword(text)} />
                                {isPasswordValid != null &&
                                    <MaterialCommunityIcons name={isPasswordValid?'check-circle': 'close-circle'} color={isPasswordValid?'blue': 'red'} size={20} style={{paddingRight: 5}} />
                                }
                            </Item>
                            {errorSubmit?.password && <Text style={[style.fieldError]}>{errorSubmit.password}</Text> }
                        </View>
                    </Form>
                    <View style={{flexDirection: 'row', marginBottom: 30}}>
                        <CheckBox checked={rememberMeChecked} style={{marginRight: 16}} onPress={() => {this.setState({rememberMeChecked: !rememberMeChecked})} } />
                        <Text onPress={() => {this.setState({rememberMeChecked: !rememberMeChecked})} }>Ingatkan saya  </Text>
                    </View>
                    {errorSubmit?.result  &&
                        <Card style={{marginBottom: 30}}>
                            <CardItem header bordered>
                                <Left>
                                    <MaterialCommunityIcons name="close-circle" color="red" size={20} style={{paddingRight: 5}} />
                                    <Body>
                                        <Text >Data tidak valid:</Text>
                                    </Body>
                                </Left>
                            </CardItem>
                            <CardItem bordered>
                                <Body>
                                    <Text style={{ color: 'red'}}>  {errorSubmit.result}</Text>
                                </Body>
                            </CardItem>
                        </Card>
                    }

                    <View>
                        <Button  style={style.buttonBrown} full  onPress={() => this.submitData()}>
                            <Text style={{textTransform: 'capitalize'}}>Masuk</Text>
                        </Button>
                    </View>
                    <View style={{textAlign: 'center', marginTop: 30}}>
                        <Text style={[style.link, {textAlign: 'center'}]} onPress={() => this.props.navigation.navigate('ForgotPassword')}>Lupa password?</Text>
                    </View>

                </Content>
            </Container>
        );
    }
}

