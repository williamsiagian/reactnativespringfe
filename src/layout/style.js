import {StyleSheet} from 'react-native'
// import url('https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap');
// <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet">
export default StyleSheet.create({
	heading1: {
        fontSize: 64
    },
    heading2: {
        fontSize: 48,
    },
    heading3: {
        fontSize: 36
    },
    heading4: {
        fontSize: 24,
    },
    heading5: {
        fontSize: 20
    },
    heading6: {
        fontSize: 18,
        fontWeight: 'bold'
    },
    formLabel: {
        fontSize: 14,
        fontWeight: 'bold',
        marginLeft: 5
    },
    formInput: {
        fontSize: 14,
        color: 'rgb(103, 103, 103)',
        flex: 1 


    },

    button50: {
        flexDirection: 'column', 
        justifyContent: 'center', 
        paddingLeft: 10, 
        paddingRight: 10
    },

    link: {
        color: 'rgb(0, 175, 179)',
        fontSize: 12,
        fontFamily: 'Open Sans'
    },
    roundedInput: {
        borderRadius: 8,
        marginTop: 8,
        marginBottom: 24,
    },
    fieldError: {
       marginTop: -20, 
       marginBottom: 24, 
       marginLeft: 10, 
       color: 'red', 
       fontSize: 14
    },
    colorlight: {
        color: '#31353b'
    },
    buttonBrown: {
        backgroundColor: '#FAB726',
        color: 'white',
        borderRadius: 8,
    },
    buttonWhite: {
        backgroundColor: 'white',
        borderRadius: 8,
        borderColor: '#FAB726'
    },
    textButtonWhite: {
        color: '#FAB726',
        
    },
    whiteBox: {
        backgroundColor: 'white',
        borderRadius: 4,
        shadowColor: "#000",
        shadowOffset: {
            width: 2,
            height: 0,
        },
        shadowOpacity: 0.4,
        shadowRadius: 4,
        elevation: 3,
        borderWidth: 0.5,
        borderColor: 'rgba(232, 232, 232,0.4)'
    },
    productTitle: {
        color: 'rgb(0, 0, 0)',
        fontSize: 14,
        fontFamily: 'Open Sans Bold',
        fontWeight: 'bold',
        marginVertical: 15
    },
    productPrice: {
        color: 'rgb(250, 182, 42)',
        fontSize: 14,
        fontFamily: 'Open Sans',
        fontWeight: "bold",
        textAlign: 'left'
    },
    
    
    imageProduct: {
        height: 113,
        width: 157,
        resizeMode: 'center',
        marginBottom: 5,
        backgroundColor: 'white',

    },
    blurText: {
        color: 'rgb(180, 180, 180)',
        textShadowColor: 'rgb(0, 0, 0)',
        textShadowOffset: {width: 4, height: 0},
        textShadowRadius: 10,
        shadowOpacity: 0.9
    },
    modalBox: {
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20
    }
});

export const greyColor = 'rgb(180, 180, 180)';
export const headColor = 'rgb(0, 122, 122)';
export const loadingIcon = require('./../../assets/icon/loading.gif');
export const noImageIcon = require('./../../assets/icon/no-image.png');

