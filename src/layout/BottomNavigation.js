import React, { Component,useContext, useEffect, useState }  from 'react';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import IconBadge from 'react-native-icon-badge';

import { Container, Content, Text, View } from "native-base";
import { NavigationContainer, StackActions } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import AsyncStorage from '@react-native-community/async-storage';
import { BottomTabBar } from '@react-navigation/bottom-tabs'
import AuthContext,{ AuthProvider } from "./../providers/AuthProvider";
import { notificationProvider } from "./../providers/NotificationProvider";
import {transactionProvider}  from "./../providers/TransactionProvider";

import { fcmService } from "./../notification/FCMService";
import { localNotificationService } from "./../notification/LocalNotificationService";


import {HomeScreen, BrandScreen, ProductScreen, ProductDetailScreen, 
        TransactionScreen, TransactionDetailScreen, InvoiceScreen, InvoiceDetailScreen, PaymentScreen, PostFeedbackScreen,
        ProfileScreen, LoginScreen, SignupScreen, ProfileInfoScreen,
        LoggedOutScreen, OtpVerifyScreen, SignupSuccessScreen, NotificationScreen, EmailVerificationScreen,
        ForgotPasswordScreen, ForgotPasswordOtpVerifyScreen, 
        ChangePasswordScreen, ChangePasswordSuccessScreen, 
        SearchResultScreen, LoyaltyPointScreen, WishListScreen, EditProfileScreen, ArticleDetailScreen,
        CartScreen, CheckOutScreen} from './../screens';



        

const Tab = createBottomTabNavigator();

const StackHome = createStackNavigator();
const StackLoggedOut = createStackNavigator();
const StackProduct = createStackNavigator();
const StackTransaction = createStackNavigator();
const StackNotification = createStackNavigator();
const StackProfile = createStackNavigator();
const StackSearch = createStackNavigator();
const StackCart = createStackNavigator();

/*const getActiveRouteState = function (route: NavigationState): NavigationState {
    if (!route.routes || route.routes.length === 0 || route.index >= route.routes.length) {
        return route;
    }

    const childActiveRoute = route.routes[route.index] as NavigationState;
    return getActiveRouteState(childActiveRoute);
}
*/

function getDeepestRoute(route) {
  if (!route.routes) return route.routeName;
  return getDeepestRoute(route.routes[route.index]);
}

function HomeStack({navigation,route}) {
    return (
        <StackHome.Navigator
            initialRouteName="Home"
            screenOptions={{
                headerStyle: { backgroundColor: '#42f44b' },
                headerTintColor: '#fff',
                headerTitleStyle: { fontWeight: 'bold' }
            }} >
            {<StackHome.Screen name="Home" component={HomeScreen}  options={{headerShown: false}} />}
            {<StackHome.Screen name="Brand" component={BrandScreen}  options={{headerShown: false}} />}
            {<StackProfile.Screen name="ArticleDetail" component={ArticleDetailScreen} options={{ headerShown : false}}/>}
        </StackHome.Navigator>
    );
}

function TransactionStack({navigation,route}) {
    return (
        <StackTransaction.Navigator
            initialRouteName="Transaction" >
            {<StackTransaction.Screen name="Transaction" component={TransactionScreen} options={{headerShown: false}}/>}
            {<StackTransaction.Screen name="TransactionDetail" component={TransactionDetailScreen} options={{headerShown: false}}/>}
            {<StackTransaction.Screen name="Invoice" component={InvoiceScreen} options={{headerShown: false}}/>}
            {<StackTransaction.Screen name="InvoiceDetail" component={InvoiceDetailScreen} options={{headerShown: false}}/>}
            {<StackTransaction.Screen name="Payment" component={PaymentScreen} options={{headerShown: false}}/>}
            {<StackTransaction.Screen name="PostFeedback" component={PostFeedbackScreen} options={{headerShown: false}}/>}
        </StackTransaction.Navigator>
    );
}


function ProductStack({navigation,route}) {

    return (
        <StackProduct.Navigator
            initialRouteName="Product" >
            {<StackProduct.Screen name="Product" component={ProductScreen}  options={{headerShown: false}} />}
            {<StackProduct.Screen name="ProductDetail" component={ProductDetailScreen}  options={{headerShown: false}}  />}
        </StackProduct.Navigator>
    );
}


function NotificationStack({navigation,route}) {
    return (
        <StackNotification.Navigator
            initialRouteName="Notification"
            headerMode={'none'} >
            {<StackNotification.Screen name="Notification" component={NotificationScreen} />}
        </StackNotification.Navigator>
    );
}

function ProfileStack({navigation,route}) {

    return (
        <StackProfile.Navigator
            initialRouteName="Profile"
            screenOptions={{
                headerStyle: { backgroundColor: '#42f44b' },
                headerTintColor: '#fff',
                headerTitleStyle: { fontWeight: 'bold' }
            }}>
            {<StackProfile.Screen name="Profile" component={ProfileScreen} options={{headerShown : false}}/>}
            {<StackProfile.Screen name="LoyaltyPoint" component={LoyaltyPointScreen} options={{ headerShown : false }}/>}
            {<StackProfile.Screen name="WishList" component={WishListScreen} options={{ headerShown : false }}/>}
            {<StackProfile.Screen name="ProfileInfo" component={ProfileInfoScreen} options={{ headerShown : false}}/>}
            {<StackProfile.Screen name="EditProfile" component={EditProfileScreen} options={{headerShown: false}} />}
           
        </StackProfile.Navigator>
    );
}

function LoggedOutStack({navigation,route}) {
    /*setTimeout(() => {
        if (route.state && route.state.index> 0){
            navigation.setOptions({tabBarVisible: false});
        } else {
            navigation.setOptions({tabBarVisible: true});
        }

    },100)*/

    return (
        <StackLoggedOut.Navigator
            initialRouteName="LoggedOut"
             >
            {<StackLoggedOut.Screen name="LoggedOut" component={LoggedOutScreen} options={{headerShown: false}} />}
            {<StackLoggedOut.Screen name="Login" component={LoginScreen}  options={{headerShown: false}} />}
            {<StackLoggedOut.Screen name="Signup" component={SignupScreen} options={{headerShown: false}} />}
            {<StackLoggedOut.Screen name="EmailVerification" component={EmailVerificationScreen} options={{headerShown: false}} />}
            {<StackLoggedOut.Screen name="OtpVerify" component={OtpVerifyScreen}  options={{headerShown: false}} />}
            {<StackLoggedOut.Screen name="ForgotPassword" component={ForgotPasswordScreen}  options={{headerShown: false}} />}
            {<StackLoggedOut.Screen name="ForgotPasswordOtpVerify" component={ForgotPasswordOtpVerifyScreen}  options={{headerShown: false}} />}
            {<StackLoggedOut.Screen name="ChangePassword" component={ChangePasswordScreen}  options={{headerShown: false}} />}
            {<StackLoggedOut.Screen name="ChangePasswordSuccess" component={ChangePasswordSuccessScreen}  options={{headerShown: false}} />}
            {<StackLoggedOut.Screen name="SignupSuccess" component={SignupSuccessScreen}  options={{headerShown: false}} />}

        </StackLoggedOut.Navigator>
    );
}

function RegisterFinishStack({navigation,route}) {

    return (
        <StackSearch.Navigator initialRouteName="Survey">
            {<StackLoggedOut.Screen name="SignupSuccess" component={SignupSuccessScreen}  options={{headerShown: false}} />}
        </StackSearch.Navigator>
    );
}


function SearchStack({navigation,route}) {

    return (
        <StackSearch.Navigator
            initialRouteName="SearchResult"
             >
            {<StackSearch.Screen name="SearchResult" component={SearchResultScreen} options={{headerShown: false}} />}

        </StackSearch.Navigator>
    );
}

function CartStack({navigation,route}) {

    return (
        <StackCart.Navigator
            initialRouteName="Cart"
             >
            {<StackCart.Screen name="Cart" component={CartScreen} options={{headerShown: false}} />}
            {<StackCart.Screen name="CheckOut" component={CheckOutScreen} options={{headerShown: false}} />}

        </StackCart.Navigator>
    );
}

const TabConfig = () => {
    const { userToken, unreadNotification, getUnreadNotificationTotal, userCartCount, getUserCartCount } = useContext(AuthContext); 

    useEffect(()=>{
        fcmService.registerAppWithFCM();
        fcmService.register(onRegister, onNotification, onOpenNotification);
        localNotificationService.configure(onOpenNotification);
        getUnreadNotificationTotal();
        getUserCartCount();
    },[])
    
    
    const onRegister = (token) => {
        console.log("[App] Token", token);
        if(token){
            AsyncStorage.setItem('fcm_token', token);
            console.log('Masuk FCM Token 2')
        }else{
            console.log('Error FCM token', token)
        }
    }
    
    const onNotification = (notify) => {
        const options = {
            soundName: 'default',
            playSound: true,
        }
        console.log('notify', notify);
        getUnreadNotificationTotal();
        localNotificationService.showNotification(
            0,
            notify.notification.title,
            notify.notification.body,
            notify,
            options,
        )
    }
    
    const onOpenNotification = async (notify) => {
        console.log('notify', notify);
        // {Navigation.navigate('NotificationScreen')}
    }

    return (
        <NavigationContainer>
            <Tab.Navigator   
                screenOptions={({ route }) => ({tabBarButton: ["SearchStack", "CartStack", "RegisterFinishStack"].includes(route.name) ? () => {return null; } : undefined, })}
                initialRouteName="HomeStack" tabBarOptions={{ activeTintColor: 'rgb(0, 175, 179)'}} >

                <Tab.Screen
                    name="HomeStack"
                    component={HomeStack}
                    options={{
                        tabBarLabel: 'Beranda',
                        tabBarIcon: ({ color, size }) => (
                            <MaterialCommunityIcons name="home" color={color} size={size} />
                        ),
                    }}
                />

                <Tab.Screen
                    name="ProductStack"
                    component={ProductStack}
                    options={{
                        tabBarLabel: 'Produk',
                        tabBarIcon: ({ color, size }) => (
                            <MaterialCommunityIcons name="view-grid-outline" color={color} size={size} />
                        ),
                    }} 
                    listeners={({ navigation, route }) => ({
                        tabPress: e => {
                            navigation.navigate('ProductStack', {screen: 'Product', params: {reload: false}});
                        },
                    })}  
                />
                <Tab.Screen
                    name="TransactionStack"
                    component={TransactionStack}
                    options={{
                        tabBarLabel: 'Pesanan',
                        tabBarIcon: ({ color, size }) => (
                            <MaterialCommunityIcons name="view-list" color={color} size={size} />
                        ),
                    }} 
                    listeners={({ navigation, route }) => ({
                        tabPress: e => {
                            navigation.navigate('TransactionStack', {screen: 'Transaction', params: {reload: false}});
                        },
                    })}  

                />
                <Tab.Screen
                    name="NotificationStack"
                    component={NotificationStack}
                    options={{
                        tabBarLabel: 'Notifikasi',
                        tabBarIcon: ({ color, size }) => (
                            <IconBadge
                                MainElement={
                                  <View style={{width: size, height: size, margin: 6 }} >
                                        <MaterialCommunityIcons name="bell-outline" color={color} size={size} />
                                  </View>
                                }
                                BadgeElement={
                                  <Text style={{color:'#FFFFFF', fontSize: 10}}>{unreadNotification}</Text>
                                }
                                IconBadgeStyle={
                                  {width:20,
                                  height:20,
                                  backgroundColor: 'rgb(0, 175, 179)'}
                                }
                                Hidden={!unreadNotification}
                            />
                        ),
                    }}
                />
                {userToken != null ? 
                    (<Tab.Screen
                        name="ProfileStack"
                        component={ProfileStack}
                        options={{
                            tabBarLabel: 'Akun',
                            tabBarIcon: ({ color, size }) => (
                                <MaterialCommunityIcons name="account-outline" color={color} size={size} />
                            ),
                        }}
                        listeners={({ navigation, route }) => ({
                        tabPress: e => {
                            navigation.navigate('ProfileStack', {screen: 'Profile', params: {}});
                        },
                    })}
                    />)
                :
                    (<Tab.Screen
                        name="LoggedOutStack"
                        component={LoggedOutStack}
                        options={{
                            tabBarLabel: 'Akun',
                            tabBarIcon: ({ color, size }) => (
                                <MaterialCommunityIcons name="account-outline" color={color} size={size} />
                            ),
                            // tabBarVisible:false
                        }}
                        listeners={({ navigation, route }) => ({
                            tabPress: e => {
                                if (route.state && route.state.index>0 ) {
                                  navigation.navigate('LoggedOut');
                                }
                            },
                        })}  
                    />)
                }

                <Tab.Screen
                    name="SearchStack"
                    component={SearchStack}
                    options={{
                        tabBarVisible:false
                        
                    }}
                />
                <Tab.Screen
                    name="CartStack"
                    component={CartStack}
                    options={{
                        tabBarVisible:false
                        
                    }}
                />
                <Tab.Screen
                    name="RegisterFinishStack"
                    component={RegisterFinishStack}
                    options={{
                        tabBarVisible:false
                        
                    }}
                />
            </Tab.Navigator>
        </NavigationContainer>
    );
}

const BottomNav = () => (
    <AuthProvider>
        <TabConfig />
    </AuthProvider>
);

export default BottomNav;