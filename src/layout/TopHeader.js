import React, { useContext }  from 'react';
import { View  } from 'react-native';

import { Container, Header, Left, Body, Right, Button, Title, Text } from 'native-base';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { useNavigation } from '@react-navigation/native';

import style, {headColor, greyColor} from './style';

const TopHeader = (props) => {
    const navigation = useNavigation();
    const goBack =() => {
        navigation.goBack();
    }
    return (
        <View style={{height: 80, backgroundColor: headColor,paddingTop: 32, paddingHorizontal: 20, flexDirection: 'row', alignItems: 'center'}}>
            <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start', flex: 1}}>
                {!props.noBackButton?
                    <Button  transparent onPress={goBack}>
                        <MaterialCommunityIcons name="arrow-left" size={24} color={'white'}  />
                    </Button>
                :
                    <Text style={{color: 'white', fontSize: 16, fontWeight: 'bold'}}>{props.title}</Text>
                }

                {props.skipButton && 
                    <Text style={{color: 'white', textTransform: 'capitalize'}} onPress={props.iconClick.bind(props.parent, "skipButton")}>Skip </Text>
                }

            </View>
            {!props.noBackButton &&
                <View style={{flexDirection: 'row', justifyContent: 'flex-start', flex: 4}}>
                        <Text style={{color: 'white', fontSize: 16, fontWeight: 'bold'}}>{props.title}</Text>
                </View>
            }
            <View style={{flexDirection: 'row', justifyContent: 'flex-end', flex: 1}}>
                {props.iconRight?.map((item) => {
                    return (
                        <Button style={{marginLeft: 20}} transparent key={item.icon} onPress={props.iconClick.bind(props.parent, item.code)}>
                            <MaterialCommunityIcons name={item.icon} size={24} color={(item.color)? item.color: 'white'}   />
                        </Button>
                    )
                }) }
            </View>
        </View>
    )
}

export default TopHeader;