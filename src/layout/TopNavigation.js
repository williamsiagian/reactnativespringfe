import React, { useState, useEffect, useRef, useContext }  from 'react';

import { Header, Icon, Body, Item, Button, Text, View } from "native-base";
import { Col, Row, Grid } from 'react-native-easy-grid';
import { useNavigation } from '@react-navigation/native';
import {  TextInput, Keyboard, TouchableOpacity} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import IconBadge from 'react-native-icon-badge';

import AuthContext,{ AuthProvider } from "./../providers/AuthProvider";
import Loader from './Loader';
import {productProvider}  from "./../providers/ProductProvider";
import style, {headColor, greyColor} from './style';

const TopNav = (props) => {
    const navigation = useNavigation();
    const searchInput = useRef();
    const { userToken, searchText, setSearchText, userCartCount } = useContext(AuthContext); 

    const goBack =() => {
        setSearchText('');

        Keyboard.dismiss();
        navigation.navigate('HomeStack');
    }

    const searchSubmit = async (e) => {
        navigation.navigate('SearchStack', {screen: 'SearchResult', params: {searchText: searchText}})

    }

    const gotoCart =() => {
        if (userToken) {
            navigation.navigate('CartStack', {screen: 'Cart'});
        } else {
            navigation.navigate('LoggedOutStack', {screen: 'LoggedOut'});
        }
    }
    
    const gotoWishList =() => {
        if (userToken) {
            navigation.navigate('ProfileStack', {screen: 'WishList', params: {reload: false}});
        } else {
            navigation.navigate('LoggedOutStack', {screen: 'LoggedOut'});
        }
    }


    return (
        <View transparent style={{height: 'auto', paddingHorizontal: 20}}>
            <View style={{marginVertical: 15, flexDirection: 'row', justifyContent: 'space-between'}}>
                <View style={{height: 50, flex: 1}} >
                    <Item style={{backgroundColor: "white", paddingHorizontal: 10, borderRadius: 4}} >
                        <Icon name='search' style={{color: greyColor}} />
                        <TextInput 
                            placeholder='Cari spare part motor disini' 
                            onChange={(event) => {setSearchText(event.nativeEvent.text) }} 
                            ref={searchInput} 
                            returnKeyType="search" 
                            onSubmitEditing={()=>{searchSubmit() }}
                            style={{flex: 1}}
                            value={searchText}
                        />
                        {props.searchMode && <Icon name='close'  onPress={() => goBack() } /> }

                    </Item>

                </View>
                <TouchableOpacity style={{ width: 45, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center'}}  onPress={() => gotoCart() }>
                    <IconBadge 
                        MainElement={
                          <View style={{width: 24, height: 24, margin: 10 }} >
                                <MaterialCommunityIcons name="cart-outline" color={'white'} size={24} />
                          </View>
                        }
                        BadgeElement={
                          <Text style={{color:headColor, fontSize: 10, fontWeight: 'bold'}}>{userCartCount}</Text>
                        }
                        IconBadgeStyle={
                          {width:20,
                          height:20,
                          backgroundColor: 'white'}
                        }
                        Hidden={!userToken || userCartCount===null }
                    />
                </TouchableOpacity>
                <TouchableOpacity style={{width: 30, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center'}}  onPress={() => gotoWishList() }>
                    <MaterialCommunityIcons name="table-heart" color={'white'} size={24}   />
                </TouchableOpacity>

            </View>
        </View>
    )
}

export default TopNav;