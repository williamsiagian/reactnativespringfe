import React from "react";
import {HttpService} from './HttpService';
import AsyncStorage from '@react-native-community/async-storage';

class AuthProvider {

    postLogin = async () => {
        const result = await HttpService.post(`/login`);
        return result;
    }

    postRegister = async () => {
        const result = await HttpService.post(`/register`);
        return result;
    }


}

export const notificationProvider = new AuthProvider()

