import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';

const timeout = 30000; // timeout in ms
const API_URL = "http://localhost:8080/api";


class HTTP_SERVICE  {

    async setConfig(){
        const token = await AsyncStorage.getItem("userToken");
        console.log('api token', token);
        return {
            'headers': {
                'Authorization': `Bearer ${token}`,
            },
            'timeout': timeout
        }
    }

    get = async (url) => {
        const config = await this.setConfig();
        var urlPost;
        
        if (url.indexOf("http") == -1 ) {
            urlPost = `${API_URL}${url}` ;
        } else {
            urlPost = url;
        }
        console.log('url get', urlPost);
        return axios.get(urlPost, config).then(response => {
            if (response.status == 200) {
                return response.data;
            } else {
                return {status: response.status, message: "error", data: null}
            }
        })
        .catch(error => {
            if (error.response) {
                console.log('err response', error.response.status);
                return {status: error.response.status, message: error.response.message, data: error.response.data}
            } else {
                return {status: 500, message: "error", data: null}

            }
        });
    }

    post = async (url, request) => {
        const config = await this.setConfig();
        var urlPost;

        if (url.indexOf("http") == -1 ) {
            urlPost = `${API_URL}${url}` ;
        } else {
            urlPost = url;
        }
        console.log('request', request);
        return axios.post(urlPost, request, config).then(response => {
            if (response.status == 200 || response.status == 201 || response.status == 204) {
                return response.data;
            } else {
                return {status: response.status, message: "error", data: null}
            }
        })
        .catch(error => {
            if (error.response) {
                console.log('err response', error.response.status);
                return {status: error.response.status, message: error.response.message, data: error.response.data}
            } else {
                return {status: 500, message: "error", data: null}
            }
            
        });
    }

    upload = async (url, request) => {
        const config = await this.setConfig();
        config.headers['Content-Type'] = "multipart/form-data";
        var urlPost;

        if (url.indexOf("http") == -1 ) {
            urlPost = `${API_URL}${url}` ;
        } else {
            urlPost = url;
        }
        console.log('request', request);
        return axios.post(urlPost, request, config).then(response => {
            if (response.status == 200) {
                return response.data;
            } else {
                return {status: response.status, message: "error", data: null}
            }
        })
        .catch(error => {
            if (error.response) {
                console.log('err response', error.response.status);
                return {status: error.response.status, message: error.response.message, data: error.response.data}
            } else {
                return {status: 500, message: "error", data: null}
            }
            
        });
    }


    delete = async (url) => {
        const config = await this.setConfig();
        var urlPost;

        if (url.indexOf("http") == -1 ) {
            urlPost = `${API_URL}${url}` ;
        } else {
            urlPost = url;
        }
        return axios.delete(urlPost, config).then(response => {
            if (response.status == 200) {
                return response.data;
            } else {
                return {status: response.status, message: "error", data: null}
            }
        })
        .catch(error => {
            if (error.response) {
                console.log('err response', error.response.status);
                return {status: error.response.status, message: error.response.message, data: error.response.data}
            } else {
                return {status: 500, message: "error", data: null}
            }
            
        });
    }



};
export const HttpService = new HTTP_SERVICE()
